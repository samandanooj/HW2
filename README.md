This is code to demonstrate unit testing 
capabilities with a c++ calculator program.

The Visual Studio directory contains files that open
as a Solution and should be built and tested as such.

The Source Code directory simply contains the
.cpp and .h files of the program.