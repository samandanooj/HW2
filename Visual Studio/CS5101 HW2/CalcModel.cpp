#include "CalcModel.h"

CalcModel::CalcModel(){}

CalcModel::~CalcModel(){}

int CalcModel::add(int int1, int int2){
  return int1 + int2;
}

int CalcModel::subtract(int int1, int int2){
  return int1 - int2;
}

int CalcModel::multiply(int int1, int int2){
  return int1 * int2;
}

int CalcModel::divide(int int1, int int2){
  return int1 / int2;
}