#include <iostream>
#include <string>
using namespace std;

#pragma once
#ifndef VIEW_H
#define VIEW_H
class CalcView{
  public:
    CalcView();
    ~CalcView();
    void getInput();
    int getInt1();
    int getInt2();
    char getOper();
    void print(string s);
  private:
    int int1, int2;
    char oper;
};

#endif