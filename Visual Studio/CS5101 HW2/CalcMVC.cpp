#include "CalcController.h"
#include "CalcModel.h"
#include "CalcView.h"
#include <iostream>
using namespace std;

int main(){
  CalcView view;
  CalcModel model;
  CalcController control(view, model);
  control.run();
}


