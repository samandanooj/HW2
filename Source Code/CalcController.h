#include "CalcModel.h"
#include "CalcView.h"

#pragma once
#ifndef CONTROL_H
#define CONTROL_H

class CalcController{
  public:
    CalcController();
    CalcController(CalcView v, CalcModel m);
    ~CalcController();
    void run();
  private:
    CalcView view;
    CalcModel model;
};

#endif