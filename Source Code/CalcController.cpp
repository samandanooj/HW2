#include "CalcController.h"

CalcController::CalcController(){}

CalcController::CalcController(CalcView v, CalcModel m){
  view = v;
  model = m;
}

CalcController::~CalcController(){}

void CalcController::run(){
  while (true){
    view.getInput();
    char oper = view.getOper();
    switch (oper){
      case '+':
        view.print(to_string(model.add(view.getInt1(), view.getInt2())));
        break;
      case '-':
        view.print(to_string(model.subtract(view.getInt1(), view.getInt2())));
        break;
      case '*':
        view.print(to_string(model.multiply(view.getInt1(), view.getInt2())));
        break;
      case '/':
        view.print(to_string(model.divide(view.getInt1(), view.getInt2())));
        break;
      default:
        view.print("Not a valid operator");
        break;
    }
  }
}