#include "CalcView.h"

CalcView::CalcView(){
  int1 = 0;
  int2 = 0;
  oper = '+';
}

CalcView::~CalcView(){}

void CalcView::getInput(){
  cout << endl << "Enter the operation you would like to solve (ex. INT + INT)" << endl;
  cin >> int1;
  cin >> oper;
  cin >> int2;
}

int CalcView::getInt1(){
  return int1;
}

int CalcView::getInt2(){
  return int2;
}

char CalcView::getOper(){
  return oper;
}

void CalcView::print(string s){
  cout << s << endl;
}