#include "stdafx.h"
#include "CppUnitTest.h"
#include "../CS5101 HW2/CalcModel.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CalcUnitTest
{		
	TEST_CLASS(CalcModelTest)
	{
	public:
		
		TEST_METHOD(TestAdd)
		{
      CalcModel model;
      int actual = model.add(5, 5);
      int expected = 10;
      Assert::AreEqual(expected, actual);
		}

    TEST_METHOD(TestSubtract)
    {
      CalcModel model;
      int actual = model.subtract(10, 5);
      int expected = 5;
      Assert::AreEqual(expected, actual);
    }

    TEST_METHOD(TestMultiply)
    {
      CalcModel model;
      int actual = model.multiply(10, 5);
      int expected = 50;
      Assert::AreEqual(expected, actual);
    }

    TEST_METHOD(TestDivide)
    {
      CalcModel model;
      int actual = model.divide(10, 5);
      int expected = 2;
      Assert::AreEqual(expected, actual);
    }

	};
}