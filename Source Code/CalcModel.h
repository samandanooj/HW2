#pragma once
#ifndef MODEL_H
#define MODEL_H
class CalcModel{
  public:
    CalcModel();
    ~CalcModel();
    int add(int int1, int int2);
    int subtract(int int1, int int2);
    int multiply(int int1, int int2);
    int divide(int int1, int int2);
};

#endif